import json

from requests import get

from utils.ai_devs_api_client import get_auth_token, get_task, post_response
from utils.chat_gpt_api_client import Message, send_messages, function_calling

# Wykonaj zadanie API o nazwie ‘knowledge’. Automat zada Ci losowe pytanie na temat kursu walut, populacji wybranego
# kraju lub wiedzy ogólnej. Twoim zadaniem jest wybór odpowiedniego narzędzia do udzielenia odpowiedzi (API z wiedzą
# lub skorzystanie z wiedzy modelu). W treści zadania uzyskanego przez API, zawarte są dwa API, które mogą być dla
# Ciebie użyteczne.

# {
#     'code': 0,
#     'msg': 'I will ask you a question about the exchange rate, the current population or general knowledge. Decide '
#            'whether you will take your knowledge from external sources or from the knowledge of the model',
#     'question': 'ile orientacyjnie ludzi mieszka w Polsce?',
#     'database #1': 'Currency http://api.nbp.pl/en.html (use table A)',
#     'database #2': 'Knowledge about countries https://restcountries.com/ - field 'population''
# }


tools = [
    {
        'type': 'function',
        'function': {
            'name': 'get_population',
            'description': 'Get population of given country',
            'parameters': {
                'type': 'object',
                'properties': {
                    'parameter': {
                        'type': 'string',
                        'description': 'Name of country, e.g. poland, france, germany, usa',
                    }
                },
                'required': ['parameter']
            },
        }
    },
    {
        'type': 'function',
        'function': {
            'name': 'get_exchange_price',
            'description': 'Get exchange price for given currency',
            'parameters': {
                'type': 'object',
                'properties': {
                    'parameter': {
                        'type': 'string',
                        'description': 'Code of currency, e.g. usd, eur, jpy',
                    }
                },
                'required': ['parameter']
            },
        }
    },
    {
        'type': 'function',
        'function': {
            'name': 'answer_question',
            'description': 'Answer general question witch are not about population nor currency',
            'parameters': {
                'type': 'object',
                'properties': {
                    'parameter': {
                        'type': 'string',
                        'description': 'Repeat given question',
                    }
                },
                'required': ['parameter']
            },
        }
    }
]


def get_population(country: str) -> int:
    country_data = get(f'https://restcountries.com/v3.1/name/{country}').json()[0]
    return country_data['population']


def get_exchange_price(currency: str) -> float:
    currency_data = get(f'https://api.nbp.pl/api/exchangerates/rates/c/{currency}').json()
    return currency_data['rates'][0]['ask']


def answer_question(question: str) -> str:
    system = Message('system', 'Answer questions as truthfully and briefly as possible.')
    user = Message('user', question)
    assistant = send_messages([system, user])
    return assistant.choices[0].message.content


token = get_auth_token('knowledge')
question = get_task(token)['question']
print(question)
user = Message('user', question)
function = function_calling([user], tools)
arguments = json.loads(function.arguments)
# getattr to call method from other object
response = globals()[function.name](arguments['parameter'])
print(response)
post_response(response, token)

# problemy
# kurs euro, dolara
