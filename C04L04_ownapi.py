from config import get_lucy_ai_devs_2_url
from utils.ai_devs_api_client import get_auth_token, post_response

# Rozwiąż zadanie API o nazwie ‘ownapi’. Zadanie polega na zbudowaniu prostego API webowego, które będzie odpowiadać
# na pytania zadawane przez nasz system sprawdzania zadań. Adres URL do API działającego po HTTPS prześlij w polu
# ‘answer’ do endpointa /answer/. System na wskazany adres URL wyśle serię pytań (po jednym na żądanie). Swoje API
# możesz hostować na dowolnej platformie no-code (Make/N8N), jak i na własnym serwerze VPS, czy hostingu
# współdzielonym. Jeśli nadal komunikacja z API jest niejasna, przeczytaj hinta https://zadania.aidevs.pl/hint/ownapi

api = F'{get_lucy_ai_devs_2_url()}/ownapi'
token = get_auth_token('ownapi')
post_response(api, token)
