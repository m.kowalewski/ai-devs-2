from config import get_lucy_ai_devs_2_url
from utils.ai_devs_api_client import get_auth_token, post_response

# Rozwiąż zadanie API o nazwie ‘md2html’. Naszym celem jest stworzenie za pomocą fine-tuningu modelu,
# który po otrzymaniu pliku Markdown na wejściu, zwróci jego sformatowaną w HTML wersję. Jako bazę do nauki użyj
# modelu gpt-3.5-turbo. Załóż, że musisz obsłużyć jedynie znaczniki takie jak nagłówki (3 stopnie), pogrubienie,
# kursywa, podkreślenie oraz cytat. Tak stworzony model musisz podpiąć do swojego API i musisz podać nam jego adres
# URL do testów (identycznie jak w przypadku zadań typu ownapi, czy ownapipro). W razie problemów rzuć okiem do
# hintów https://zadania.aidevs.pl/hint/md2html. UWAGA: tworząc dane do nauki modelu, podaj nie tylko przykładowe
# zamiany Markdown na HTML, ale także przykłady użycia tych transformacji w prawdziwych zdaniach. Im więcej podasz
# przykładów, tym skuteczniej będzie działać nowy model.

api = f'{get_lucy_ai_devs_2_url()}/md2html'
token = get_auth_token('md2html')
post_response(api, token)
