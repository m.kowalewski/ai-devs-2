from requests import post

from config import get_render_form_key
from utils.ai_devs_api_client import get_auth_token, get_task, post_response

# Wykonaj zadanie API o nazwie “meme”. Celem zadania jest nauczenie Cię pracy z generatorami grafik i dokumentów.
# Zadanie polega na wygenerowaniu mema z podanego obrazka i podanego tekstu. Mem ma być obrazkiem JPG o wymiarach
# 1080x1080. Powinien posiadać czarne tło, dostarczoną grafikę na środku i podpis zawierający dostarczony tekst.
# Grafikę z memem możesz wygenerować za pomocą darmowych tokenów dostępnych w usłudze RenderForm (50 pierwszych
# grafik jest darmowych). URL do wygenerowanej grafiki spełniającej wymagania wyślij do endpointa /answer/. W razie
# jakichkolwiek problemów możesz sprawdzić hinty https://zadania.aidevs.pl/hint/meme

# {
#     'code': 0,
#     'msg': 'Create meme using RednerForm API and send me the URL to JPG via /answer/ endpoint',
#     'service': 'https://renderform.io/',
#     'image': 'https://zadania.aidevs.pl/data/monkey.png',
#     'text': 'Gdy koledzy z pracy mówią, że ta cała automatyzacja to tylko chwilowa moda, a Ty właśnie zastąpiłeś ich '
#             'jednym, prostym skryptem',
#     'hint': 'https://zadania.aidevs.pl/hint/meme'
# }

API_URL = 'https://get.renderform.io/api/v2/render'
TEMPLATE_ID = 'boring-whales-wait-daily-1820'

HEADERS = {
    'Content-Type': 'application/json',
    'X-API-KEY': get_render_form_key()
}

token = get_auth_token('meme')
task = get_task(token)

payload = {
    "template": TEMPLATE_ID,
    "data": {
        "memas.src": task['image'],
        "content.text": task['text']
    }
}

url = post(API_URL, headers=HEADERS, json=payload).json()['href']
print(url)
post_response(url, token)
