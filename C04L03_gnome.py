from utils.ai_devs_api_client import get_auth_token, get_task, post_response
from utils.chat_gpt_api_client import Message, send_message_with_image

# Rozwiąż zadanie API o nazwie ‘gnome’. Backend będzie zwracał Ci linka do obrazków przedstawiających gnomy/skrzaty.
# Twoim zadaniem jest przygotowanie systemu, który będzie rozpoznawał, jakiego koloru czapkę ma wygenerowana postać.
# Uwaga! Adres URL zmienia się po każdym pobraniu zadania i nie wszystkie podawane obrazki zawierają zdjęcie postaci
# w czapce. Jeśli natkniesz się na coś, co nie jest skrzatem/gnomem, odpowiedz “error”.
# Do tego zadania musisz użyć GPT-4V (Vision).

# {
#     'code': 0,
#     'msg': 'I will give you a drawing of a gnome with a hat on his head. Tell me what is the color of the hat in '
#            'POLISH. If any errors occur, return \'ERROR\' as answer',
#     'hint': 'it won\'t always be a drawing of a gnome >:)',
#     'url': 'https://zadania.aidevs.pl/gnome/16fdfe293e71aa197c6229b6985a7012.png'
# }

token = get_auth_token('gnome')
url = get_task(token)['url']
print(url)

red_url = 'https://zadania.aidevs.pl/gnome/16fdfe293e71aa197c6229b6985a7012.png'
green_url = 'https://zadania.aidevs.pl/gnome/414c7e2fcd1e1c9c923e939084819e36.png'
troll_url = 'https://zadania.aidevs.pl/gnome/08d8c505c496f224458ce6032760f440.png'

system_prompt = (
    'Twoim zadaniem jest powiedzenie w jakim kolorze jest czapka skrzata (inaczej gnoma) na obrazku. \n'
    'Zasady: \n'
    ' - odpowiadasz jednym słowem którym jest kolor czapki gnoma \n'
    ' - jeśli na obrazku jest co innego niż gnom odpowiadasz "error"\n'
)
system = Message('system', system_prompt)
user_prompt = [
    {
        "type": "text",
        "text": "Jaki jest kolor czapki?"
    },
    {
          "type": "image_url",
          "image_url": {
            "url": url,
          },
    }
]
user = Message('user', user_prompt)
response = send_message_with_image([system, user])
print(response)
post_response(response, token)
