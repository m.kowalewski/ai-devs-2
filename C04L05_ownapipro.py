from requests import post
from config import get_lucy_ai_devs_2_url
from utils.ai_devs_api_client import get_auth_token, post_response

# Rozwiąż zadanie API o nazwie “ownapipro”. Jak nazwa wskazuje, jest to rozbudowana wersja wczorajszego zadania (
# możesz więc wykorzystać wczoraj napisany kod!). Przebieg zadania jest identyczny jak w przypadku ownapi,
# z tą tylko różnicą, że system sprawdzający nie zawsze zadaje pytania, a czasami przekazuje informacje, które musisz
# zapamiętać. Przykładowo, podczas jednej rozmowy system przekaże informację „Mieszkam w Krakowie”, a w kolejnej
# zapyta “Gdzie mieszkam?”. Musisz więc obsłużyć zarówno wiedzę ogólną modelu, jak i wiedzę przekazaną do modelu w
# trakcie rozmowy (pamięć stała). Utrudnieniem jest fakt, że dane przekazywane do zapamiętania, zmieniają się przy
# każdym uruchomieniu testu. Dodatkowe hinty do zadania https://zadania.aidevs.pl/hint/ownapipro

api = f'{get_lucy_ai_devs_2_url()}/ownapipro'
post(f'{api}/start-new-converstion')
token = get_auth_token('ownapipro')
post_response(api, token)
