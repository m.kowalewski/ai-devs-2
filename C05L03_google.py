from config import get_lucy_ai_devs_2_url
from utils.ai_devs_api_client import get_auth_token, post_response

# Rozwiąż zadanie API o nazwie ‘google’. Do jego wykonania będziesz potrzebować darmowego konta w usłudze SerpAPI.
# Celem zadania jest samodzielne zaimplementowanie rozwiązania podobnego do tego, znanego z ChatGPT Plus,
# gdzie po wpisaniu zapytania na temat, o którym model nie ma pojęcia, uruchamiana jest wyszukiwarka BING. My
# posłużymy się wyszukiwarką Google, a Twój skrypt będzie wyszukiwał odpowiedzi na pytania automatu sprawdzającego i
# będzie zwracał je w czytelnej dla człowieka formie. Więcej informacji znajdziesz w treści zadania /task/,
# a podpowiedzi dostępne są pod https://zadania.aidevs.pl/hint/google.


api = f'{get_lucy_ai_devs_2_url()}/google'
token = get_auth_token('google')
post_response(api, token)
