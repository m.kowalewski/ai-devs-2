import json

from requests import get
from datetime import datetime

from utils.ai_devs_api_client import get_auth_token, get_task, post_response
from utils.chat_gpt_api_client import Message, send_messages, function_calling

# Rozwiąż zadanie API o nazwie ‘tools’. Celem zadania jest zdecydowanie, czy podane przez API zadanie powinno zostać
# dodane do listy zadań (To Do), czy do kalendarza (jeśli ma ustaloną datę). Oba narzędzia mają lekko definicje
# struktury JSON-a (różnią się jednym polem). Spraw, aby Twoja aplikacja działała poprawnie na każdym zestawie
# danych testowych.

# {
#     'code': 0,
#     'msg': 'Decide whether the task should be added to the To Do list or to the calendar (if time is provided) and '
#            'return the corresponding JSON',
#     'hint': 'always use YYYY-MM-DD format for dates',
#     'example for To Do': 'Przypomnij mi, że mam kupić mleko = {'tool':'ToDo','desc':'Kup mleko' }',
#     'example for Calendar': 'Jutro mam spotkanie z Marianem = {'tool':'Calendar','desc':'Spotkanie z '
#                             'Marianem','date':'2023-11-20'}',
#     'question': 'Przypomnij mi, abym zapisał się na AI Devs 3.0'
# }


tools = [
    {
        'type': 'function',
        'function': {
            'name': 'ToDo',
            'description': 'Add task to todolist. Must be apply for all task without given date',
            'parameters': {
                'type': 'object',
                'properties': {
                    'desc': {
                        'type': 'string',
                        'desc': 'Briefly task description',
                    }
                },
                'required': ['desc']
            },
        }
    },
    {
        'type': 'function',
        'function': {
            'name': 'Calendar',
            'description': 'Add event to calendar. Must be apply for all event and task with given date',
            'parameters': {
                'type': 'object',
                'properties': {
                    'desc': {
                        'type': 'string',
                        'description': 'Code of currency, e.g. usd, eur, jpy',
                    },
                    'date': {
                        'type': 'string',
                        'description': 'Date of task as string in format yyyy-MM-dd e.g. 2023-11-20',
                    }
                },
                'required': ['desc', 'date']
            },
        }
    }
]

token = get_auth_token('tools')
question = get_task(token)['question']
print(question)
current_date_time = datetime.now()
formatted_date = current_date_time.strftime('%Y-%m-%d')
system = Message('system', f'context ```\n - today is {formatted_date}\n```')
user = Message('user', question)
function = function_calling([system, user], tools)
response = {
    'tool': function.name,
    'desc': json.loads(function.arguments)['desc']
}
if function.name == 'Calendar':
    response['date'] = json.loads(function.arguments)['date']
print(json.dumps(response, indent=4))
post_response(response, token)
pass
