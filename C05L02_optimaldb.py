from utils.ai_devs_api_client import get_auth_token, post_response

# Rozwiąż zadanie API o nazwie ‘optimaldb’. Masz dostarczoną bazę danych o rozmiarze ponad 30kb.
# https://zadania.aidevs.pl/data/3friends.json Musisz zoptymalizować ją w taki sposób, aby automat korzystający z
# niej, a mający pojemność pamięci ustawioną na 9kb był w stanie odpowiedzieć na 6 losowych pytań na temat trzech
# osób znajdujących się w bazie. Zoptymalizowaną bazę wyślij do endpointa /answer/ jako zwykły string. Automat użyje
# jej jako fragment swojego kontekstu i spróbuje odpowiedzieć na pytania testowe. Wyzwanie polega na tym,
# aby nie zgubić żadnej informacji i nie zapomnieć kogo ona dotyczy oraz aby zmieścić się w wyznaczonym limicie
# rozmiarów bazy.

# {
#     "code": 0,
#     "msg": "In a moment you will receive from me a database on three people. It is over 30kb in size. You need to "
#            "prepare me for an exam in which I will be questioned on this database. Unfortunately, the capacity of my "
#            "memory is just 9kb. Send me the optimised database",
#     "database": "https://zadania.aidevs.pl/data/3friends.json",
#     "hint": "I will use GPT-3.5-turbo to answer all test questions"
# }

token = get_auth_token('optimaldb')
with open('C05L02_optimaldb_prompt.txt', 'r') as file:
    prompt = file.read()
post_response(prompt, token)
