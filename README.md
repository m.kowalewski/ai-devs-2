# AI Devs 2 Course Project
This repository contains solutions for tasks in the [AI Devs 2](https://aidevs.pl) course.

## Table of Contents
1. [Table of Contents](#table-of-contents) 
2. [Technologies](#technologies)
3. [Setup](#setup)
4. [Runnable files](#runnable-files)
5. [Modules](#modules)

## Technologies
 - Python 3.11

## Setup

To get started with this project, follow these steps:

### Install required packages

**openai**

``` bash
pip install openai
```

**requests**

``` bash
pip install requests
```

**numpy**

``` bash
pip install numpy
```

**beautifulsoup4**
For web scrapping

``` bash
pip install beautifulsoup4
```

**selenium**
For web scrapping

``` bash
pip install selenium
```

**qdrant-client**
For web scrapping

``` bash
pip install qdrant-client
```

### Add config file

In the project root directory, add a config.json file following this schema:

```json
{
    "ai_devs_api_key": "AI_DEVS_API_KEY",
    "open_ai_api_key": "OPEN_AI_API_KEY"
}
```

Replace "AI_DEVS_API_KEY" and "OPEN_AI_API_KEY" with your actual API keys.

## Runnable files

For each lesson is one file with examples or homework:
 - C01L01.py

Other runnable files:
 - 

## Modules

### Utils

The utils module contains various helper classes and functions to assist with the project. These utilities can be used across different parts of the project to simplify code and improve maintainability.

Utils contains:
 - Client for AI Deva API `ai_devs_api_client.py`
 - Authorization helper AI Deva API `ai_devs_auth_util.py`
